# Example Tests

A simple example using `cunit` to test the circular buffer solution
to ac-401 exercise 4.

Copy the `src` and `tesst` directories in a cmake host project
and run the command:

```
./build.sh reset test
```

from within that project.

You can also run the tests directly from the command line or
via `ctest`:

```
$ cd build/debug
$ tests/test-buffer
$ ctest --output-on-failure
```

