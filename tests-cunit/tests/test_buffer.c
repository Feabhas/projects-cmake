// test_buffer.c
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

/*
Test circular buffer from ac-401 chaprter 4
*/

#include <CUnit/Basic.h>
#include "buffer.h"
#include <stdio.h>

#define TEST_INFO(func) {#func, func}

static BUFFER buffer;

static void test_insert_one_value(void)
{
    CU_ASSERT(buffer_is_empty(buffer));
    int n = 1;
    CU_ASSERT(BUFFER_OK == buffer_add(buffer, &n));
    CU_ASSERT(! buffer_is_empty(buffer));

    int m;
    CU_ASSERT(BUFFER_OK == buffer_get(buffer, &m));
    CU_ASSERT(n == m);
    CU_ASSERT(buffer_is_empty(buffer));
}

static void test_insert_two_values(void)
{
    CU_ASSERT(buffer_is_empty(buffer));
    int n = 1;
    int m = 2;
    CU_ASSERT(BUFFER_OK == buffer_add(buffer, &n));
    CU_ASSERT(BUFFER_OK == buffer_add(buffer, &m));
    CU_ASSERT(! buffer_is_empty(buffer));

    int r;
    CU_ASSERT(BUFFER_OK == buffer_get(buffer, &r));
    CU_ASSERT(r == n);
    CU_ASSERT(BUFFER_OK == buffer_get(buffer, &r));
    CU_ASSERT(r == m);
    CU_ASSERT(buffer_is_empty(buffer));
}

static void test_fill_buffer(void)
{
    CU_ASSERT(buffer_is_empty(buffer));
    for (int i = 1; i <= BUFFER_SIZE; ++i) {
        CU_ASSERT(BUFFER_OK == buffer_add(buffer, &i));
    }
    int n = 0;
    CU_ASSERT(BUFFER_FULL == buffer_add(buffer, &n));

    for (int i = 1; i <= BUFFER_SIZE; ++i) {
        CU_ASSERT(BUFFER_OK == buffer_get(buffer, &n));
        CU_ASSERT(n == i);
    }
    CU_ASSERT(buffer_is_empty(buffer));
}

static void test_buffer_wraparound(void)
{
    CU_ASSERT(buffer_is_empty(buffer));
    for (int i = 1; i <= BUFFER_SIZE; ++i) {
        CU_ASSERT(BUFFER_OK == buffer_add(buffer, &i));
    }
    int n = 0;
    for (int i=-3; i<0; ++i) {
        CU_ASSERT(BUFFER_OK == buffer_get(buffer, &n));
        CU_ASSERT(BUFFER_OK == buffer_add(buffer, &i));
    }
    for (int i = 1; i <= BUFFER_SIZE; ++i) {
        CU_ASSERT(BUFFER_OK == buffer_get(buffer, &n));
    }
    CU_ASSERT(buffer_is_empty(buffer));
    CU_ASSERT(n == -1);
}

static CU_TestInfo tests[] = {
    TEST_INFO(test_insert_one_value),
    TEST_INFO(test_insert_two_values),
    TEST_INFO(test_fill_buffer),
    TEST_INFO(test_buffer_wraparound),
    CU_TEST_INFO_NULL,
};

static CU_SuiteInfo suites[] = {
  { .pName="Buffer tests", .pTests=tests },
  CU_SUITE_INFO_NULL,
};

int main(void)
{
    buffer = buffer_init();

    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();

    if (CUE_SUCCESS != CU_register_suites(suites)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    unsigned count = CU_get_number_of_tests_failed();
    CU_cleanup_registry();
    return (int)count;
}
