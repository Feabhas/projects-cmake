// buffer.c
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#include <assert.h>
#include <stddef.h>
#include "buffer.h"

struct Buffer
{
	Value_t data[BUFFER_SIZE];
	int     write;
	int     read;
	int     num_items;
};


static BUFFER get_instance(void)
{
	static struct Buffer buffers[NUM_BUFFERS];
	static unsigned int next = 0;

	if(next == NUM_BUFFERS) return NULL;

	return &buffers[next++];
}


BUFFER buffer_init(void)
{
	BUFFER buffer = get_instance();
	buffer->write = 0;
	buffer->read  = 0;
	buffer->num_items = 0;

	return buffer;
}


Buffer_error_t buffer_add(BUFFER buf, const void * const in_val)
{
	assert(buf != NULL);

	if(buf->num_items == BUFFER_SIZE) return BUFFER_FULL;

	buf->data[buf->write] = *(Value_t*)in_val;
	buf->num_items++;
	buf->write++;
	if(buf->write == BUFFER_SIZE) buf->write = 0;

	return BUFFER_OK;
}


Buffer_error_t buffer_get(BUFFER buf, void * const inout_val)
{
	assert(buf != NULL);

	if(buf->num_items == 0) return BUFFER_EMPTY;

	*(Value_t*)inout_val = buf->data[buf->read];
	buf->num_items--;
	buf->read++;
	if(buf->read == BUFFER_SIZE) buf->read = 0;

	return BUFFER_OK;
}


bool buffer_is_empty(BUFFER buf)
{
	assert(buf != NULL);

	return(buf->num_items == 0);
}


const char* buffer_error_as_string(Buffer_error_t error)
{
	static const char *strings[] =
	{
		"OK",
		"Buffer is full",
		"Buffer is empty"
	};

	return strings[error];
}
