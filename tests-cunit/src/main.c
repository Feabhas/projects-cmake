// main.c
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#include <stdio.h>
#include "buffer.h"


int main(void)
{
  BUFFER buffer = buffer_init();
  Buffer_error_t error;

  // Last insertion should fail...
  //
  for(int i = 0; i < BUFFER_SIZE + 1; i++)
  {
    printf("Inserting: %d\n", i);
    error = buffer_add(buffer, &i);
    puts(buffer_error_as_string(error));
  }

  // half fill
  //
  for(int i = 0; i < (BUFFER_SIZE/2); i++)
  {
    Value_t val = 0;
    error = buffer_get(buffer, &val);
    printf("retrieving: %d\n", val);
    puts(buffer_error_as_string(error));
  }

  for(int i = 0; i < (BUFFER_SIZE/2) - 1; i++)
  {
    printf("Inserting: %d\n", i);
    error = buffer_add(buffer, &i);
    puts(buffer_error_as_string(error));
  }

  // Last retrieval should fail
  //
  for(int i = 0; i < BUFFER_SIZE + 1; i++)
  {
    Value_t val = 0;
    error = buffer_get(buffer, &val);
    printf("retrieving: %d\n", val);
    puts(buffer_error_as_string(error));
  }
}
