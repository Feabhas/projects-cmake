// buffer.h
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#pragma once
#ifndef BUFFER_H
#define BUFFER_H

#include <stdbool.h>

#define BUFFER_SIZE     8
#define NUM_BUFFERS 	2


// Pointer-to-opaque-type idiom
//
typedef struct Buffer *BUFFER;

typedef enum { BUFFER_OK, BUFFER_FULL, BUFFER_EMPTY } Buffer_error_t;

// Type alias, so we can hold different
// types in the buffer; although only
// one type in each system.
//
typedef int Value_t;

BUFFER buffer_init(void);
Buffer_error_t buffer_add(BUFFER buf, const void * const in_val);
Buffer_error_t buffer_get(BUFFER buf, void * const inout_val);
bool buffer_is_empty(BUFFER buf);
const char* buffer_error_as_string(Buffer_error_t error);


#endif
