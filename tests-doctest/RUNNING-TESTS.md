# Build and run tests

This example is using `doctest` as a test framework using QEMU to emulate the target

The example `CMakeLists.txt` is using `ExternalProject_Add` to clone doctest from `github`. This could be replaced by making `doctest` a submodule or located outside of the project.

To build and run, simply use "
```
./build.sh test
```

If any drivers are being used from `driver-cpp` then the library will need adding for linkage in `CMakeLists.txt`
e.g.
```
target_link_libraries(Application PRIVATE drivers-cpp)
```