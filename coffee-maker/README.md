# Coffee Maker Demonstration

This is an alternate simulation using the same Cortex M4 Discovery board
in the Washing Machine Simulator.

The same pin out assignments for input (0-7) and output (8-15) have
been used to simulate a coffee maker machine based on a typical 
domestic coffee pod machine.

The `05 - Coffee Maker.doc` contains a project description for th
coffee machine.

# Overview

The coffee maker emulator has no operational functionality other 
than providing animated graphics and has the following features:

   1.	A coffee selector panel with up/down arrows to select a coffee style and a start button to initiate the coffee brewing process.
   2. A display panel to show the selected coffee style.
   3.	An animated coffee cup showing coffee being brewed or milk added to the coffee.
   4.	A heater icon showing when the milk heater is enabled.
   5.	A steam icon showing when the milk is being foamed.
   6.	A toggle button used to remove or replace a coffee cup
   7.	A icon to display a warning when the cup is not present (no icon is 
   shown when a cup is present as shown above)

Each coffee style is defined by a series of steps (a recipe) that that last for a short duration and manipulate an output pin to simulate behaviour. The four steps are:
   * brewing coffee
   * heating milk
   * foaming milk
   * adding milk

The different coffee recipes are:
   1.	Ristretto: brew coffee (3 secs)
   2.	Espresso: brew coffee (5 secs)
   3.	Americano: brew coffee (7 secs)
   4.	Café au lait: americano + add milk (2 secs)
   5.	Flat white: americano + heat milk (2 secs) + add milk (2 secs)
   6.	Flat white: espresso + heat milk (2 secs) + foam milk (2 secs) + add milk (2 secs)
   7.	Flat white: espresso + heat milk (2 secs) + foam milk (3 secs) + add milk (2 secs)

# Hardware Configuration

## Input pins

   * 0 Up - Used to select the next coffee recipe, selection should be changed when the pin latches high, then wait for the pin to drop to zero 
   * 1 Down - Used to select the previous coffee recipe, wait for pin to latch high then drop back to zero
   * 2 Start - Used to start the coffee brewing recipe, wait for pin to latch high then drop back to zero
   * 3 No-Cup - Pin is low to show a cup is present and high when cup is absent; the button toggles between the high/low state each time the GUI button is clicked

## Output pins

   * 8 Led A (Panel select) - Set high to enable the controller board LED A
and change the coffee panel selection
   * 9 Led B (Panel select) - Set high to enable the controller board LED B
and change the coffee panel selection
   * 10 Led C (Panel select) - Set high to enable the controller board LED C
and change the coffee panel selection
   * 11 Led D
Brew coffee - Set high to enable the controller board LED D 
High to enable coffee brewing step, low to stop
   * 12 Heater - High to enable milk heating step, low to stop
   * 13 Steamer - High to enable milk foaming step, low to stop
   * 14 Add milk - High to enable adding milk step, low to stop
   * 15 Cup warning - High to illuminate the missing cup icon, low to hide the icon

Notes
   * Pins 8-10 represent the 8 coffee selection values: zero is the “select coffee style” message and represents no selection, values 1..7 represent the coffee recipes previously discussed

# Example C++ application

An example C++ application has the following components:

   1.	`Selector` class to mange the up/down and start buttons and manage the missing coffee cup warning icon
   2.	`Step` abstract base class to manage the timing delay 
   3.	`BrewStep` to enable/disable the coffee brewing step and animated coffee cup
   4.	`HeatStep` to enable/disable the animated the heater icon
   5.	Foa`mStep to enable/disable the animated the steamer icon
   6.	`MilkStep` to enable/disable the add milk step and animated coffee cup
   7.	`CoffeeRecipe` to store `std::unique_ptr<Step>` objects
   8.	`CoffeeMaker` to create and run each coffee recipe
