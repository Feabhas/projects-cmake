// WashProgramme.cpp
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#include <iostream>
#include "CoffeeRecipe.h"
#include "Step.h"
#include "Timer.h"

namespace Cafe 
{
    bool CoffeeRecipe::add(std::unique_ptr<Step> step)
    {
        if (next == std::end(steps)) {
            return false;
        }
        *next = std::move(step);
        ++next;
        return true;
    }

    void CoffeeRecipe::run()
    {
        for (auto&& step: steps) {
            if (step) {
                std::cout << "Recipe step " << unsigned(step->get_type()) << '\n';
                step->run();
            }
        }
    }

}  // namespace Cafe
