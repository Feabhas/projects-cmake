// WashProgramme.h
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#pragma once
#ifndef COFFEEMAKER_H
#define COFFEEMAKER_H

#include <array>
#include "Brewer.h"
#include "Milker.h"
#include "CoffeeStyle.h"
#include "CoffeeRecipe.h"

namespace Cafe {
    class CoffeeMaker {
    public:
        CoffeeMaker(Devices::GPIO& gpio);
        void run(CoffeeStyle style);

    private:
        constexpr static unsigned num_styles { 8 };
        using container = std::array<CoffeeRecipe, num_styles>;
        container recipes { };

        Devices::Brewer brewer;
        Devices::Milker milker;
    };

}  // namespace Cafe

#endif // COFFEEMAKER_H
