// Motor.h
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#pragma once
#ifndef SELECTOR_H
#define SELECTOR_H

#include "GPIO.h"
#include "CoffeeStyle.h"

using Cafe::CoffeeStyle;

namespace Devices
{
    class Selector
    {
    public:
        Selector(GPIO &gpiod);

        auto select() -> CoffeeStyle;
        void set_style(CoffeeStyle style);
        auto cup_removed() -> bool;

    private:
        GPIO& gpio;
        uint32_t selection {};
    };
}

#endif // SELECTOR_H
