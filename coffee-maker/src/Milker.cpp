// Milker.cpp
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#include "Milker.h"

namespace {
    void update_pin(Devices::GPIO& gpio, uint32_t pin, bool state) 
    {
        if (state) gpio.set(0x1u << pin);
        else gpio.clear(0x1u << pin);
    }
}
namespace Devices
{
    enum class MilkPins {heat=12, foam, add};

    Milker::Milker(GPIO& gpio)
    : gpio{gpio}
    {
        for (auto pin = uint32_t(MilkPins::heat); pin <= uint32_t(MilkPins::add); pin++) {
            gpio.set_output(pin);
        }
    }

    Milker::~Milker()
    {
        heat(false);
        foam(false);
        add(false);
    }

    void Milker::heat(bool state)
    {
        update_pin(gpio, uint32_t(MilkPins::heat), state);
    }
    
    void Milker::foam(bool state)
    {
        update_pin(gpio, uint32_t(MilkPins::foam), state);
    }

    void Milker::add(bool state)
    {
        update_pin(gpio, uint32_t(MilkPins::add), state);
    }

}  // namespace Devices

