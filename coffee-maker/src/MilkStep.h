// MilkStep.h
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#pragma once
#ifndef MILKSTEP_H
#define MILKSTEP_H

#include "Milker.h"
#include "Step.h"

using Devices::Milker;

namespace Cafe
{
  class MilkStep : public Step {
  public:
    MilkStep(Milker& brwr, Type step_type, uint32_t step_length);

    void run() override;
    
  private:
    Milker& milker;
  };

} // namespace Cafe

#endif // MILKSTEP_H
