// Brewer.h
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#pragma once
#ifndef BREWER_H
#define BREWER_H

#include "GPIO.h"
#include "Step.h"

namespace Devices
{
    class Brewer
    {
    public:
        Brewer(GPIO &gpiod);
        ~Brewer();
        Brewer(Brewer&& gpiod) = delete;

        void on();
        void off();

    private:
        GPIO& gpio;
    };
}

#endif // BREWER_H
