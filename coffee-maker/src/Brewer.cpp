// Brewer.cpp
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#include "Brewer.h"

namespace Devices
{
    constexpr uint32_t brew_pin {11};

    Brewer::Brewer(GPIO& gpio)
    : gpio{gpio}
    {
        gpio.set_output(brew_pin);
    }

    Brewer::~Brewer()
    {
        off();
    }

    void Brewer::on()
    {
        gpio.set(0x1u << brew_pin);
    }

    void Brewer::off()
    {
        gpio.clear(0x1u << brew_pin);
    }

}  // namespace Devices

