// Motor.cpp
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#include "Selector.h"

using Cafe::CoffeeStyle;

namespace Devices
{
    struct Range {
        uint32_t begin;
        uint32_t end;
    };

    static constexpr Range button_pins{0, 3};
    static constexpr Range panel_pins{8, 10};

    enum class Button: uint32_t {up=1u, down=2u, start=4u, no_cup=8u};
    constexpr uint32_t button_mask {0xffu};
    constexpr uint32_t no_cup_warning {15};

    constexpr Range styles{uint32_t(CoffeeStyle::none), uint32_t(CoffeeStyle::cappuccino)};

    Selector::Selector(GPIO &gpio)
    : gpio{gpio}
    {
        for (auto pin=button_pins.begin; pin <= button_pins.end; pin++) {
            gpio.set_input(pin);
        }
        for (auto pin=panel_pins.begin; pin <= panel_pins.end; pin++) {
            gpio.set_output(pin);
        }
        gpio.set_output(no_cup_warning);
        set_style(CoffeeStyle::none);
    }

    static bool get_button_state(GPIO& gpio, Button button)
    {
        auto buttons = gpio.read() & button_mask;        
        if (buttons & uint32_t(button)) {
            while(gpio.read() & uint32_t(button))
            {}
            return true;
        }
        return false;
    }

    auto Selector::select() -> CoffeeStyle
    {
        bool cup {true};

        for(;;) {
            auto buttons = gpio.read() & button_mask;
            if (buttons == 0) {
                if (not cup) {
                    gpio.clear(0x1u << no_cup_warning);
                    cup = true;  
                }
                continue;
            }

            if (get_button_state(gpio, Button::up)) {
                selection = (selection < styles.end) ? (selection + 1) : styles.begin;
                set_style(CoffeeStyle(selection));
            }
            else if (get_button_state(gpio, Button::down)) {
                selection = (selection > styles.begin) ? (selection - 1) : styles.end;
                set_style(CoffeeStyle(selection));
            }
            else if (get_button_state(gpio, Button::start)) {
                if ((buttons & uint32_t(Button::no_cup)) != 0) {
                    if (cup) {
                        gpio.set(0x1u << no_cup_warning); 
                        cup = false;                   
                    }
                }
                if (selection != styles.begin and cup) {
                    break;
                }
            }        
        }
        return CoffeeStyle(selection);
    }

    void Selector::set_style(CoffeeStyle style)
    {
        gpio.clear_set(0x7u << panel_pins.begin, uint32_t(style) << panel_pins.begin);
    }

    auto Selector::cup_removed() -> bool
    {
        return (gpio.read() & uint32_t(Button::no_cup)) != 0;
    }

}  // namespace Devices

