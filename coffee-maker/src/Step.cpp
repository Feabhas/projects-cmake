// Step.cpp
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#include "Step.h"
#include "Timer.h"
#include <iomanip>
#include <iostream>

namespace Cafe
{
  namespace {
    static constexpr std::string_view names[9] = {
      "invalid", "dispencee", "fill",
      "heat",    "wash",  "rinse",
      "spin",    "dry",   "complete"
    };
  }

  Step::Step(Type step_type, uint32_t step_length) :
    type{ step_type }, duration{ step_length }
  {  }

  void Step::wait()
  {
    display();
    sleep(duration);
  }

  constexpr const char* Step::to_string(Type type)
  {
    switch(type) {
      case Type::invalid:   return "invalid";      
      case Type::brew:      return "brew";      
      case Type::heat_milk: return "heat_milk";
      case Type::foam_milk: return "foam_milk";
      case Type::add_milk:  return "add_milk";
    }
    return "**error**";
  }

  void Step::display() const
  {
    std::cout << std::fixed << std::setprecision(2);

    std::cout << "Step " << int(type) <<  " '" << to_string(type) << "' ";
    std::cout << "running for " << duration/1000.0 << " seconds";
    std::cout << '\n';
  }

} // namespace WMS
