// WashProgramme.h
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#pragma once
#ifndef COFFEERECIPE_H
#define COFFEERECIPE_H

#include <array>
#include <memory>
#include "Step.h"

namespace Cafe {
    class CoffeeRecipe {
    public:
        bool add(std::unique_ptr<Step> step);
        void run();

    private:
        constexpr static unsigned num_steps { 8 };
        using container = std::array<std::unique_ptr<Step>, num_steps>;
        container steps { };
        container::iterator next { std::begin(steps) };
    };

}  // namespace Cafe

#endif // COFFEERECIPE_H_
