// Step.cpp
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#include "BrewStep.h"

namespace Cafe
{
  BrewStep::BrewStep(Brewer& brwr, Type step_type, uint32_t step_length) :
    Step{ step_type, step_length }, brewer {brwr}
  {  }

  void BrewStep::run()
  {
    brewer.on();
    Step::wait();
    brewer.off();
  }

} // namespace WMS
