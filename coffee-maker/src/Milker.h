// Milker.h
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#pragma once
#ifndef MILKER_H
#define MILKER_H

#include "GPIO.h"
#include "Step.h"

namespace Devices
{
    class Milker
    {
    public:
        Milker(GPIO &gpiod);
        ~Milker();
        Milker(Milker&& gpiod) = delete;

        void heat(bool state);
        void foam(bool state);
        void add(bool state);
        
    private:
        GPIO& gpio;
    };
}

#endif // MILKER_H
