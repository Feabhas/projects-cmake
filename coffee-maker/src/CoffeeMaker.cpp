// WashProgramme.cpp
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#include <memory>
#include "CoffeeMaker.h"
#include "BrewStep.h"
#include "MilkStep.h"
#include "Timer.h"

namespace {
    using Cafe::Step;

    auto make_brew_step(Devices::Brewer& brewer, uint32_t duration) 
    {
        return std::make_unique<Cafe::BrewStep>(brewer, Step::Type::brew, duration);
    }

    auto make_milk_step(Devices::Milker& milker, Step::Type type, uint32_t duration) 
    {
        return std::make_unique<Cafe::MilkStep>(milker, type, duration);
    }

}

namespace Cafe {
    CoffeeMaker::CoffeeMaker(Devices::GPIO& gpio)
    : brewer{gpio}, milker{gpio}
    {  
        auto& ristretto = recipes[uint32_t(CoffeeStyle::ristretto)];
        ristretto.add(make_brew_step(brewer, 4000u));
        auto& espresso = recipes[uint32_t(CoffeeStyle::espresso)];
        espresso.add(make_brew_step(brewer, 7000u));
        auto& americano = recipes[uint32_t(CoffeeStyle::americano)];
        americano.add(make_brew_step(brewer, 10000u));
        auto& cafe_au_lait = recipes[uint32_t(CoffeeStyle::cafe_au_lait)];
        cafe_au_lait.add(make_brew_step(brewer, 7000u));
        cafe_au_lait.add(make_milk_step(milker, Step::Type::add_milk, 2000u));
        auto& flat_white = recipes[uint32_t(CoffeeStyle::flat_white)];
        flat_white.add(make_brew_step(brewer, 7000u));
        flat_white.add(make_milk_step(milker, Step::Type::heat_milk, 3000u));
        flat_white.add(make_milk_step(milker, Step::Type::add_milk, 2000u));
        auto& latte = recipes[uint32_t(CoffeeStyle::latte)];
        latte.add(make_brew_step(brewer, 4000u));
        latte.add(make_milk_step(milker, Step::Type::heat_milk, 3000u));
        latte.add(make_milk_step(milker, Step::Type::foam_milk, 2000u));
        latte.add(make_milk_step(milker, Step::Type::add_milk, 2000u));
        auto& cappuccino = recipes[uint32_t(CoffeeStyle::cappuccino)];
        cappuccino.add(make_brew_step(brewer, 4000u));
        cappuccino.add(make_milk_step(milker, Step::Type::heat_milk, 3000u));
        cappuccino.add(make_milk_step(milker, Step::Type::foam_milk, 4000u));
        cappuccino.add(make_milk_step(milker, Step::Type::add_milk, 2000u));
    }

    void CoffeeMaker::run(CoffeeStyle style)
    {
        recipes[uint32_t(style)].run();
    }

}  // namespace Cafe
