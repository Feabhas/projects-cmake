// BrewStep.h
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#pragma once
#ifndef BREWSTEP_H
#define BREWSTEP_H

#include "Brewer.h"
#include "Step.h"

using Devices::Brewer;

namespace Cafe
{
  class BrewStep : public Step {
  public:
    BrewStep(Brewer& brwr, Type step_type, uint32_t step_length);

    void run() override;
    
  private:
    Brewer& brewer;
  };

} // namespace Cafe

#endif // BREWSTEP_H
