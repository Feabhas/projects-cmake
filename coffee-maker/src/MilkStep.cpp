// Step.cpp
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#include "MilkStep.h"

namespace Cafe
{
  MilkStep::MilkStep(Milker& mlk, Type step_type, uint32_t step_length) :
    Step{ step_type, step_length }, milker {mlk}
  {  }

  void MilkStep::run()
  {
      switch(get_type()) {
        case Type::heat_milk: milker.heat(true); break;
        case Type::foam_milk: milker.foam(true); break;
        case Type::add_milk: milker.add(true); break;
        default: break;
      }
      Step::wait();
      switch(get_type()) {
        case Type::heat_milk: milker.heat(false); break;
        case Type::foam_milk: milker.foam(false); break;
        case Type::add_milk: milker.add(false); break;
        default: break;
      }  
    }

} // namespace WMS
