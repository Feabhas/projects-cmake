// Step.h
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#pragma once
#ifndef STEP_H
#define STEP_H

#include <cstdint>

namespace Cafe
{
  class Step {
  public:
    enum class Type { invalid, brew, heat_milk, foam_milk, add_milk};

    Step() = default;
    Step(Type step_type, uint32_t step_length);

    Type get_type() const { return type; }
    uint32_t get_duration() const { return duration; }
    bool is_valid() const { return type != Type::invalid; }

    static constexpr const char* to_string(Type type);
    
    virtual void run() = 0;

  protected:
    void wait();
    
  private:
    void display() const;

    Type           type {};
    uint32_t       duration {};
  };

} // namespace WMS

#endif // STEP_H_
