// main.cpp
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#include <iostream>
#include "GPIO.h"
#include "Selector.h"
#include "CoffeeMaker.h"

#include "Timer.h"
#include "Peripherals.h"

using Devices::Selector;
using Cafe::CoffeeMaker;

int main()
{
    Devices::GPIO gpiod {STM32F407::AHB1_Device::GPIO_D};
    Selector selector {gpiod};
    CoffeeMaker maker{gpiod};

    for(;;) {
        auto style = selector.select();
        std::cout << "selected " << int(style) << '\n';
        maker.run(style);
    }
    return 0;
}


enum class Button{up=1u, down=2u, start=4u, cup=8u};
enum class Pin {sel0=8, sel1, sel2, fill, heat, froth, milk};

int main_test()
{
    Devices::GPIO gpiod {STM32F407::AHB1_Device::GPIO_D};

    for (unsigned pin = int(Pin::sel0); pin <= int(Pin::milk); ++pin) {
        gpiod.set_output(pin);
    }

    gpiod.set(0x1u << int(Pin::fill));
    gpiod.set(0x1u << int(Pin::heat));
    gpiod.set(0x1u << int(Pin::froth));

    // for (unsigned i = 0; i < 8; ++i) {
    //     gpiod.clear(0x7u << int(Pin::sel0));
    //     gpiod.set(i << int(Pin::sel0));
    //     sleep(500);
    // }
    uint32_t selection{};
    while (selection != 7) {
        auto saved{selection};
        auto buttons = gpiod.read();
        if (buttons & uint32_t(Button::up)) {
            selection += 1;
            while(gpiod.read() & uint32_t(Button::up))
            {}
        }
        else if (buttons & uint32_t(Button::down)) {
            selection = (selection > 0) ? selection - 1 : 7;
            while(gpiod.read() & uint32_t(Button::down))
            {}
        }
        if (saved != selection) {
            gpiod.clear(0x7u << int(Pin::sel0));
            gpiod.set(selection << int(Pin::sel0));
        }
        sleep(100);
    }
    gpiod.clear(0x7u << int(Pin::sel0));

    gpiod.clear(0x1u << int(Pin::froth));
    gpiod.clear(0x1u << int(Pin::heat));
    gpiod.clear(0x1u << int(Pin::fill));
    return 0;
  }
