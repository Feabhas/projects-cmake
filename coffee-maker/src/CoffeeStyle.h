// WashProgramme.h
// See project README.md for disclaimer and additional information.
// Feabhas Ltd

#pragma once
#ifndef COFFEESTYLE_H
#define COFFEESTYLE_H

namespace Cafe {
    enum class CoffeeStyle {
        none, ristretto, espresso, americano, 
        cafe_au_lait, flat_white, latte, cappuccino,
    };
}  // namespace Cafe

#endif // COFFEESTYLE_H
