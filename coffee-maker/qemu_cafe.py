#!/usr/bin/python3
from enum import Enum

from qemu_wms import Button, ButtonStyle, Overlay, STM32, PinMap, Animation

class CafeBoard:
    """ Static class for GUI layout configuration """
    graphics_lib = 'graphics'
    graphics_zip = 'qemu-cafe-graphics.zip'
    board_image = 'feabhas-cafe-768.png'
    icon = 'qemu-wms-icon.png'
    image_x = 768
    image_y = 356

    checkbox_labels = ('coffee 8', 'coffee 9', 'coffee 10', 'Brew', 'Heat', 'Steam', 'Milk', 'No cup')

    buttons = [
        Button('reset', None, ButtonStyle.plain, '', 'reset ', 32, 200, 8),
        Button('up', 0, ButtonStyle.plain, 'D0l0 ', 'D0d0 ', 439, 34, 15),
        Button('down', 1, ButtonStyle.plain, 'D0l1 ', 'D0d1 ', 439, 74, 15),
        Button('start', 2, ButtonStyle.plain, 'D0l2 ', 'D0d2 ', 638, 55, 12),
        Button('no-cup', 3, ButtonStyle.toggle, 'D0l3 ', 'D0d3 ', 538, 300, 30),
    ]

    overlays = STM32.led_overlays | {
        'up': Overlay(4, 419, 18, ['up-0.png', 'up-1.png']),
        'down': Overlay(5, 419, 58, ['down-0.png', 'down-1.png']),
        'start': Overlay(6, 618, 33, ['start-0.png', 'start-1.png']),
        'no-cup': Overlay(7, 473, 302, ['cup-toggle-0.png', 'cup-toggle-1.png']),
        'selector': Overlay(8, 482, 16, [
            'select-coffee.png', 'ristretto.png', 'espresso.png', 'americano.png',
            'cafe-au-lait.png', 'flat-white.png', 'latte.png', 'cappuccino.png',
        ]),
        'brew': Overlay(9, 472, 152, ['brew-0.png', 'brew-1.png', 'brew-2.png', 'brew-3.png', 'brew-4.png']),
        'heater': Overlay(10, 328, 113, ['heater-0.png', 'heater-1.png', 'heater-2.png', 'heater-3.png']),
        'foamer': Overlay(11, 679, 109, ['steam-0.png', 'steam-1.png', 'steam-2.png', 'steam-3.png']),
        'cup-warning': Overlay(12, 334, 274, ['cup-warning-0.png', 'cup-warning-1.png']),
        'milk': Overlay(13, 472, 152, ['milk-0.png', 'milk-1.png', 'milk-2.png', 'milk-3.png', 'milk-4.png']),
        'foam': Overlay(14, 472, 152, ['foam-0.png', 'foam-1.png', 'foam-2.png', 'foam-3.png', 'foam-4.png']),
    }

    pin_map = {
        'selector': PinMap([8, 9, 10], 0,'selector'),
        'brew': PinMap([11], 0, 'brew'),
        'heat-milk': PinMap([12], 0, 'heater'),
        'foam-milk': PinMap([13], 0, 'foamer'),
        'add-milk': PinMap([14], 0, 'milk'),
        'add-foam': PinMap([14], 0, 'foam'),
        'no-cup': PinMap([15], 0, 'cup-warning'),
    }

    animation = [
        Animation('brew', 3),
        Animation('heat-milk', 3),
        Animation('foam-milk', 3),
        Animation('add-milk', 2),
        Animation('add-foam', 2),
    ]


CoffeeStage = Enum('CoffeeStage', ['selecting', 'coffee', 'milk', 'foam'])


class CafeManager:
    cup_overlays = 'brew', 'milk', 'foam'

    def __init__(self):
        self.selection = 0
        self.stage: CoffeeStage = CoffeeStage.selecting
        self.no_cup = 0
        self.end_coffee = 0
        self.foamed = False
        self.entered = {}

    def reset(self, gui):
        self.__init__()
        # self.stage = CoffeeStage.selecting
        # self.no_cup = self.end_coffee = 0
        # self.foamed = False
        # self.entered = [False] * len(CoffeeStage)
        for name in ('milk', 'foam'):
            gui.remove_image(gui.board.overlays[name])
        gui.update_image(gui.board.overlays['brew'], 0)

    def show(self, name: str):
        if name not in CafeManager.cup_overlays:
            return True
        if self.no_cup:
            return False
        if self.stage == CoffeeStage.coffee and name == 'brew':
            return True
        if self.stage == CoffeeStage.milk and name == 'milk':
            return True
        if self.stage == CoffeeStage.foam and name == 'foam':
            return True
        return False

    def set_state(self, gui, name: str, level: int, state: int):
        if name == 'selector':
            if self.stage != CoffeeStage.selecting:
                self.reset(gui)
            self.selection = state
        elif name == 'brew' and state:
            self.stage = CoffeeStage.coffee
        elif name == 'milk' and state:
            self.stage = CoffeeStage.milk if self.selection < 6 else CoffeeStage.foam
            gui.remove_image(gui.board.overlays['brew'])
        if level and name in CafeManager.cup_overlays:
            self.entered[name] = True
        if not level and self.entered.get(name, False):
            self.end_state(gui, name)

    def end_state(self, gui, name: str):
        if self.no_cup:
            return
        if name == 'brew':
            if self.selection == 1:              # ristretto
                gui.update_image(gui.board.overlays['brew'], 1)
            elif self.selection in (3, 4, 5):    # full cup
                gui.update_image(gui.board.overlays['brew'], 4)
            else:                                # espresso cup
                gui.update_image(gui.board.overlays['brew'], 2)
        elif name == 'milk':
            if self.selection == 4:
                gui.update_image(gui.board.overlays['milk'], 4)
            else:
                gui.update_image(gui.board.overlays['milk'], 3)
        elif name == 'foam':
            if self.selection == 6:
                gui.update_image(gui.board.overlays['foam'], 3)
            else:
                gui.update_image(gui.board.overlays['foam'], 4)

    def set_button(self, gui, name: str, state: int):
        if name == 'no-cup':
            self.no_cup = state
            if state:
                for image in CafeManager.cup_overlays:
                    gui.remove_image(gui.board.overlays[image])
            else:
                gui.update_image(gui.board.overlays['brew'], 0)
        elif name == 'start':
            for image in CafeManager.cup_overlays:
                gui.remove_image(gui.board.overlays[image])


def main():
    start(CafeBoard, CafeManager())


if __name__ == '__main__':
    main()
  